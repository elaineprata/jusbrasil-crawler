package com.jusbrasil.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jusbrasil.common.ProcessTO;
import com.jusbrasil.common.ProcessTOBuilder;
import com.jusbrasil.crawler.CrawlerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ProcessSearchServiceTest {

    @Mock
    private CrawlerService crawler;

    @Mock
    private ProcessSearchResultSender resultSender;

    @Mock
    private ObjectMapper objectMapper;

    private ProcessSearchService processSearchService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        processSearchService = new ProcessSearchService(crawler, resultSender, objectMapper);
    }

    @Test()
    public void findProcess_searchRequest() throws Exception {
        ProcessTO processTO = new ProcessTOBuilder().build();

        when(crawler.findProcess(any())).thenReturn(processTO);
        when(objectMapper.writeValueAsString(any())).thenReturn("");
        doNothing().when(resultSender).sendSearchResult(isA(ProcessSearchRequestTO.class), isA(ProcessTO.class));

        processSearchService.findProcess(new ProcessSearchRequestTO());

        verify(crawler, times(1)).findProcess(any());
        verify(resultSender, times(1)).sendSearchResult(any(), any());
    }
}
