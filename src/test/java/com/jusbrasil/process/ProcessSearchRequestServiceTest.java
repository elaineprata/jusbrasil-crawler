package com.jusbrasil.process;

import com.jusbrasil.message.MessageSender;
import com.jusbrasil.user.UserConfig;
import com.jusbrasil.user.UserConfigBuilder;
import com.jusbrasil.user.UserConfigService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProcessSearchRequestServiceTest {

    private ProcessSearchRequestService service;

    class MyUserConfigService extends UserConfigService {

        private UserConfig userConfig;

        public void setUserConfig(UserConfig userConfig) {
            this.userConfig = userConfig;
        }

        @Override
        public UserConfig findUserConfig(long userId) throws Exception {
            return this.userConfig;
        }
    }

    class MyMessageSender extends MessageSender {

        ProcessSearchRequestTO request;

        @Override
        public void sendMessage(ProcessSearchRequestTO request) throws Exception {
            this.request = request;
        }
    }

    @Test()
    public void createProcessSearchRequest() throws Exception {

        UserConfig userConfig = new UserConfigBuilder()
                .withWebhookUrl("http://webhook.com.br/mywebhook")
                .build();

        MyMessageSender myMessageSender = new MyMessageSender();
        MyUserConfigService myUserConfigService = new MyUserConfigService();
        myUserConfigService.setUserConfig(userConfig);

        ProcessSearchRequestService service = new ProcessSearchRequestService(myUserConfigService, myMessageSender);

        ProcessSearchRequestTO request = new ProcessSearchRequestTOBuilder().build();
        service.createProcessSearchRequest(request);

        Assertions.assertEquals("http://webhook.com.br/mywebhook", myMessageSender.request.resultUrl);
    }
}
