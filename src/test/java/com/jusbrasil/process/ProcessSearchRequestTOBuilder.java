package com.jusbrasil.process;

public class ProcessSearchRequestTOBuilder {

    long userId = 987;
    String resultUrl = "";
    String processNumber = "";

    public ProcessSearchRequestTOBuilder withUserId(long id) {
        this.userId = id;
        return this;
    }

    public ProcessSearchRequestTOBuilder withResultUrl(String url) {
        this.resultUrl = url;
        return this;
    }

    public ProcessSearchRequestTOBuilder withProcessNumber(String processNumber) {
        this.processNumber = processNumber;
        return this;
    }

    public ProcessSearchRequestTO build() {
        return new ProcessSearchRequestTO(new ProcessSearchUserRequestTO(this.processNumber), this.userId);
    }
}
