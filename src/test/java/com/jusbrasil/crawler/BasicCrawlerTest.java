package com.jusbrasil.crawler;

import com.jusbrasil.common.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BasicCrawlerTest {

    class MyBasicCrawler extends BasicCrawler {
        @Override
        public String getTRCode() {
            return null;
        }

        @Override
        protected String buildUrl(ProcessLevel level, String number) {
            return null;
        }

        @Override
        protected ProcessDataTO findProcessData(Document doc) {
            return null;
        }

        @Override
        protected PartTO findProcessAuthor(Document doc) {
            return null;
        }

        @Override
        protected PartTO findProcessDefendant(Document doc) {
            return null;
        }
    }

    class MyBasicCrawler2 extends MyBasicCrawler {

        @Override
        protected Document connect(String url) throws IOException {
            return null;
        }

        @Override
        protected void validateSearchResult(Document doc) throws ProcessNotFoundException {
        }

        @Override
        protected ProcessDataTO findProcessData(Document doc) {
            return new ProcessDataTO("number",
                                  "class",
                                  "area",
                                "subject",
                          "date",
                                 "judge",
                                "amount");
        }

        @Override
        protected PartTO findProcessAuthor(Document doc) {
            List<String> list = new ArrayList<>();
            list.add("lawyer");
            return new PartTO("author", list);
        }

        @Override
        protected PartTO findProcessDefendant(Document doc) {
            List<String> list = new ArrayList<>();
            list.add("lawyer");
            return new PartTO("defendant", list);
        }

        @Override
        protected List<MovementTO> findProcessMovements(Document doc) {
            List<MovementTO> list = new ArrayList<>();
            list.add(new MovementTO("date1", "desc1"));
            list.add(new MovementTO("date2", "desc2"));
            return list;
        }
    }

    @Test()
    public void findProcess() throws Exception {

        MyBasicCrawler2 crawler = new MyBasicCrawler2();
        ProcessDataTO process = crawler.findProcess(ProcessLevel.FIRST, "number");

        Assertions.assertEquals("number", process.getNumber());
        Assertions.assertEquals("class", process.getClazz());
        Assertions.assertEquals("area", process.getArea());
        Assertions.assertEquals("subject", process.getSubject());
        Assertions.assertEquals("date", process.getDistributionDate());
        Assertions.assertEquals("judge", process.getJudge());
        Assertions.assertEquals("amount", process.getAmount());

        Assertions.assertEquals("author", process.getAuthor().getName());
        Assertions.assertEquals("defendant", process.getDefendant().getName());

        Assertions.assertEquals(2, process.getMovementList().size());
        Assertions.assertEquals("date1", process.getMovementList().get(0).getDate());
        Assertions.assertEquals("desc1", process.getMovementList().get(0).getDescription());
        Assertions.assertEquals("date2", process.getMovementList().get(1).getDate());
        Assertions.assertEquals("desc2", process.getMovementList().get(1).getDescription());
    }

        @Test()
    public void validateSearchResult() throws Exception {

        MyBasicCrawler crawler = new MyBasicCrawler();

        String result = "<div id=\"spwTabelaMensagem\">\n" +
                "<table width=\"100%\" height=\"30\" class=\"tabelaMensagem\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "  <tr>\n" +
                "   \t<td width=\"35\" valign=\"top\" class=\"alert\"><img src=\"/cposg5/imagens/spw/icoAlert.gif\" ></td>\n" +
                "   \t<td class=\"tituloMensagem\"><b>\n" +
                "\tAtenção</b></td>  </tr>\n" +
                "</table>\n" +
                "</div>";

        Document doc = Jsoup.parse(result);
        crawler.validateSearchResult(doc);
    }


    @Test()
    public void validateSearchResult_processNotFound() {

        MyBasicCrawler crawler = new MyBasicCrawler();

        String result = "<div id=\"spwTabelaMensagem\">\n" +
                "<table width=\"100%\" height=\"30\" class=\"tabelaMensagem\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "  <tr>\n" +
                "   \t<td width=\"35\" valign=\"top\" class=\"alert\"><img src=\"/cposg5/imagens/spw/icoAlert.gif\" ></td>\n" +
                "   \t<td class=\"tituloMensagem\"><b>\n" +
                "\tAtenção</b></td>  </tr>\n" +
                "  <tr>\n" +
                "       <td class=\"alert\">&nbsp;</td>\n" +
                "       <td tabindex=\"0\" role=\"alert\" id=\"mensagemRetorno\">\n" +
                "           <li>Não existem informações disponíveis para os parâmetros informados.</li>\n" +
                "       </td>\n" +
                "  </tr>\n" +
                "</table>\n" +
                "<table width=\"100%\" border=\"0\" class=\"tabelaMensagem\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "  <tr>\n" +
                "       <td width=\"35\">&nbsp;</td>\n" +
                "       <td>&nbsp;\n" +
                "       </td>\n" +
                "  </tr>\n" +
                "</table>\n" +
                "</div>";

        Document doc = Jsoup.parse(result);

        Assertions.assertThrows(ProcessNotFoundException.class, () -> {
            crawler.validateSearchResult(doc);
        });
    }

    @Test
    public void buildParty_advogada() {
        String part = "Leidi Silva Ormond Galvão\n" +
                "Advogada:  Adriana Catelan Skowronski  \n" +
                "Advogada:  Ana Silvia Pessoa Salgado de Moura   ";

        MyBasicCrawler crawler = new MyBasicCrawler();
        PartTO partTO = crawler.buildParty(part);

        Assertions.assertEquals("Leidi Silva Ormond Galvão", partTO.getName());
        Assertions.assertNotNull(partTO.getLawyerList());
        Assertions.assertEquals(2, partTO.getLawyerList().size());
        Assertions.assertEquals("Adriana Catelan Skowronski", partTO.getLawyerList().get(0));
        Assertions.assertEquals("Ana Silvia Pessoa Salgado de Moura", partTO.getLawyerList().get(1));
    }

    @Test
    public void buildParty_mixedWords() {
        String part = "Ana Maria\n" +
                "Advogado:  Mariano Silva  \n" +
                "Advogada:  José Santos " +
                "RepreLeg: Marialdo";

        MyBasicCrawler crawler = new MyBasicCrawler();
        PartTO partTO = crawler.buildParty(part);

        Assertions.assertEquals("Ana Maria", partTO.getName());
        Assertions.assertNotNull(partTO.getLawyerList());
        Assertions.assertEquals(3, partTO.getLawyerList().size());
        Assertions.assertEquals("Mariano Silva", partTO.getLawyerList().get(0));
        Assertions.assertEquals("José Santos", partTO.getLawyerList().get(1));
        Assertions.assertEquals("Marialdo", partTO.getLawyerList().get(2));
    }

    String TABLE_MOVEMENTS = "<table>\n" +
            "<tbody style=\"display: none;\" id=\"tabelaTodasMovimentacoes\">\n" +
            "    <tr class=\"fundoClaro movimentacaoProcesso\" style=\"\">\n" +
            "        <td width=\"120\" style=\"vertical-align: top\" class=\"dataMovimentacaoProcesso\">\n" +
            "            08/10/2020\n" +
            "        </td>\n" +
            "        <td width=\"20\" valign=\"top\" aria-hidden=\"true\">\n" +
            "\n" +
            "        </td>\n" +
            "        <td style=\"vertical-align: top; padding-bottom: 5px\" class=\"descricaoMovimentacaoProcesso\">\n" +
            "\n" +
            "            Publicação de Pauta de Julgamento\n" +
            "\n" +
            "            <br/>\n" +
            "            <span style=\"font-style: italic;\">DJ &ordm; 4593 de 08 de outubro de 2020</span>\n" +
            "\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "    <tr class=\"fundoEscuro movimentacaoProcesso\" style=\"\">\n" +
            "        <td width=\"120\" style=\"vertical-align: top\" class=\"dataMovimentacaoProcesso\">\n" +
            "            07/10/2020\n" +
            "        </td>\n" +
            "        <td width=\"20\" valign=\"top\" aria-hidden=\"true\">\n" +
            "\n" +
            "            <a class=\"linkMovVincProc\" title=\"Visualizar documento em inteiro teor\" href=\"#?cdDocumento=49\" name=\"M\" cdDocumento=\"49\">\n" +
            "                <img src=\"/cposg5/imagens/doc2.gif.pagespeed.ce.39SRjpU5Tn.gif\" width=\"16\" height=\"16\" border=\"0\" />\n" +
            "            </a>\n" +
            "\n" +
            "        </td>\n" +
            "        <td style=\"vertical-align: top; padding-bottom: 5px\" class=\"descricaoMovimentacaoProcesso\">\n" +
            "\n" +
            "            <a class=\"linkMovVincProc\" title=\"Visualizar documento em inteiro teor\" href=\"#?cdDocumento=49\" name=\"M\" cdDocumento=\"49\">\n" +
            "                    Certidão de Inclusão em Pauta\n" +
            "                </a>\n" +
            "\n" +
            "            <br/>\n" +
            "            <span style=\"font-style: italic;\">SEASE - certid&atilde;o de intima&ccedil;&atilde;o</span>\n" +
            "\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "\n" +
            "    <tr class=\"fundoClaro movimentacaoProcesso\" style=\"\">\n" +
            "        <td width=\"120\" style=\"vertical-align: top\" class=\"dataMovimentacaoProcesso\">\n" +
            "            06/10/2020\n" +
            "        </td>\n" +
            "        <td width=\"20\" valign=\"top\" aria-hidden=\"true\">\n" +
            "\n" +
            "            <a class=\"linkMovVincProc\" title=\"Visualizar documento em inteiro teor\" href=\"#?cdDocumento=48\" name=\"M\" cdDocumento=\"48\">\n" +
            "                <img src=\"/cposg5/imagens/doc2.gif.pagespeed.ce.39SRjpU5Tn.gif\" width=\"16\" height=\"16\" border=\"0\" />\n" +
            "            </a>\n" +
            "\n" +
            "        </td>\n" +
            "        <td style=\"vertical-align: top; padding-bottom: 5px\" class=\"descricaoMovimentacaoProcesso\">\n" +
            "\n" +
            "            <a class=\"linkMovVincProc\" title=\"Visualizar documento em inteiro teor\" href=\"#?cdDocumento=48\" name=\"M\" cdDocumento=\"48\">\n" +
            "                    Certidão\n" +
            "                </a>\n" +
            "\n" +
            "            <br/>\n" +
            "            <span style=\"font-style: italic;\">SEASE - Certid&atilde;o de Julgamento</span>\n" +
            "\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "    </tbody>\n" +
            "</table>";
    @Test
    public void findProcessMovements() {
        MyBasicCrawler crawler = new MyBasicCrawler();

        Document doc = Jsoup.parse(TABLE_MOVEMENTS);
        List<MovementTO> movementList = crawler.findProcessMovements(doc);

        for (MovementTO mov: movementList) {
            System.out.println(mov.getDate() + " - " + mov.getDescription());
        }

        Assertions.assertNotNull(movementList);
        Assertions.assertEquals(3, movementList.size());

        MovementTO mov1 = movementList.get(0);
        Assertions.assertEquals("08/10/2020", mov1.getDate());
        Assertions.assertEquals("Publicação de Pauta de Julgamento DJ º 4593 de 08 de outubro de 2020", mov1.getDescription());

        MovementTO mov2 = movementList.get(1);
        Assertions.assertEquals("07/10/2020", mov2.getDate());
        Assertions.assertEquals("Certidão de Inclusão em Pauta SEASE - certidão de intimação", mov2.getDescription());

        MovementTO mov3 = movementList.get(2);
        Assertions.assertEquals("06/10/2020", mov3.getDate());
        Assertions.assertEquals("Certidão SEASE - Certidão de Julgamento", mov3.getDescription());
    }
}
