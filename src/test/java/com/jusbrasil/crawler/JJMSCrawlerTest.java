package com.jusbrasil.crawler;

import com.jusbrasil.common.PartTO;
import com.jusbrasil.common.ProcessDataTO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JJMSCrawlerTest {

    String BASIC_DATA_FIRST_LEVEL = "<div><div class=\"unj-entity-header__summary\">\n" +
            "    <div id=\"containerDadosPrincipaisProcesso\" class=\"container\">\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-lg-12 col-xl-13\">\n" +
            "                <!--principal -->\n" +
            "                <span id=\"numeroProcesso\" class=\"unj-larger-1\">0821901-51.2018.8.12.0001</span>\n" +
            "                <span id=\"labelSituacaoProcesso\" class=\"unj-tag\">Em grau de recurso</span>\n" +
            "                <!-- incidente -->\n" +
            "                <span class=\"unj-tag\">Há custas pendentes</span>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-lg-3 col-xl-3 mb-3\">\n" +
            "                <span id=\"labelClasseProcesso\" class=\"unj-label\">Classe</span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\"><span id=\"classeProcesso\" title=\"Procedimento Comum Cível\">Procedimento Comum Cível</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-lg-2 col-xl-3 mb-3\">\n" +
            "                <span id=\"labelAssuntoProcesso\" class=\"unj-label\">\n" +
            "                        Assunto\n" +
            "                    </span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\"> <span id=\"assuntoProcesso\" title=\"Enquadramento\">Enquadramento</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-lg-2 col-xl-2 mb-2\">\n" +
            "                <span id=\"labelForoProcesso\" class=\"unj-label\">\n" +
            "                        Foro\n" +
            "                    </span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\"> <span id=\"foroProcesso\" title=\"Campo Grande\">Campo Grande</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-lg-3 col-xl-2 mb-2\">\n" +
            "                <span id=\"labelVaraProcesso\" class=\"unj-label\">\n" +
            "                        Vara\n" +
            "                    </span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\"><span id=\"varaProcesso\" title=\"3ª Vara de Fazenda Pública e de Registros Públicos\">3ª Vara de Fazenda Pública e de Registros Públicos</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-lg-3 mb-2\">\n" +
            "                <span id=\"labelJuizProcesso\" class=\"unj-label\">Juiz</span>\n" +
            "                <div class=\"line-clamp__2\"> <span id=\"juizProcesso\" title=\"Zidiel Infantino Coutinho\">Zidiel Infantino Coutinho</span> </div>\n" +
            "            </div>\n" +
            "            <!-- Processo principal -->\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>" +
            "<div class=\"unj-entity-header__details\">\n" +
            "    <div class=\"container\">\n" +
            "        <div class=\"unj-ta-r\">\n" +
            "            <a href=\"#maisDetalhes\" class=\"unj-link-collapse\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\"maisDetalhes\">\n" +
            "                <span class=\"unj-link-collapse__show\">\n" +
            "                    <i id=\"botaoExpandirDadosSecundarios\" class=\"unj-link-collapse__icon glyph glyph-chevron-down\"></i>\n" +
            "                    Mais\n" +
            "                </span>\n" +
            "                <span class=\"unj-link-collapse__hide\">\n" +
            "                    <i id=\"botaoRecolherDadosSecundarios\" class=\"unj-link-collapse__icon glyph glyph-chevron-up\"></i>\n" +
            "                    Recolher\n" +
            "                </span>\n" +
            "            </a>\n" +
            "        </div>\n" +
            "        <div id=\"maisDetalhes\" class=\"collapse\" aria-expanded=\"false\">\n" +
            "            <div class=\"row unj-row--border-top\">\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span id=\"labelDistribuicaoProcesso\" class=\"unj-label\">Distribuição </span>\n" +
            "                    <div id=\"dataHoraDistribuicaoProcesso\">30/07/2018 às 12:39 - Automática</div>\n" +
            "                </div>\n" +
            "\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span id=\"labelControleProcesso\" class=\"unj-label\">Controle</span>\n" +
            "                    <div id=\"numeroControleProcesso\">2018/000760</div>\n" +
            "                </div>\n" +
            "\n" +
            "                <div class=\"col-lg-2 col-xl-2 mb-2\">\n" +
            "                    <span id=\"labelAreaProcesso\" class=\"unj-label\">Área</span>\n" +
            "                    <div id=\"areaProcesso\" class=\"lh-1-1 line-clamp__2\"> <span title=\"Cível\">Cível</span></div>\n" +
            "                </div>\n" +
            "\n" +
            "                <div class=\"col-lg-2 mb-2\">\n" +
            "                    <span id=\"lavelValorAcaoProcesso\" class=\"unj-label\">Valor da ação</span>\n" +
            "                    <div id=\"valorAcaoProcesso\">R$ 10.000,00</div>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>" +
            "</div>";


    @Test()
    public void findProcessData_FIRST_LEVEL() {
        Document doc = Jsoup.parse(BASIC_DATA_FIRST_LEVEL);
        TJMSCrawler crawler = new TJMSCrawler();
        ProcessDataTO to = crawler.findProcessData(doc);

        Assertions.assertEquals("0821901-51.2018.8.12.0001", to.getNumber());
        Assertions.assertEquals("Procedimento Comum Cível", to.getClazz());
        Assertions.assertEquals("Cível", to.getArea());
        Assertions.assertEquals("Enquadramento", to.getSubject());
        Assertions.assertEquals("30/07/2018 às 12:39 - Automática", to.getDistributionDate());
        Assertions.assertEquals("Zidiel Infantino Coutinho", to.getJudge());
        Assertions.assertEquals("R$ 10.000,00", to.getAmount());
    }

    String BASIC_DATA_SECOND_LEVEL = "<div>\n" +
            "<div class=\"unj-entity-header__summary\">\n" +
            "    <div class=\"container\">\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-md-13\">\n" +
            "                <!--principal -->\n" +
            "                <span class=\"unj-larger-1 \" id=\"numeroProcesso\">0821901-51.2018.8.12.0001</span>\n" +
            "                <!-- incidente -->\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\">\n" +
            "            <div class=\"col-md-3\">\n" +
            "                <span class=\"unj-label\">Classe</span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\" id=\"classeProcesso\"> <span title=\"Apelação Cível\">Apelação Cível</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-4\">\n" +
            "                <span class=\"unj-label\">\n" +
            "                        Assunto\n" +
            "                    </span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\" id=\"assuntoProcesso\"><span title=\"Obrigação de Fazer / Não Fazer\">Obrigação de Fazer / Não Fazer</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-3\">\n" +
            "                <span class=\"unj-label\">Seção</span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\" id=\"secaoProcesso\"> <span title=\"Tribunal de Justiça\">Tribunal de Justiça</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-3\">\n" +
            "                <span class=\"unj-label\">Órgão Julgador</span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\" id=\"orgaoJulgadorProcesso\"> <span title=\"4ª Câmara Cível\">4ª Câmara Cível</span></div>\n" +
            "            </div>\n" +
            "            <div class=\"col-md-2\">\n" +
            "                <span class=\"unj-label\">Área</span>\n" +
            "                <div class=\"lh-1-1 line-clamp__2\" id=\"areaProcesso\"><span title=\"Cível\">Cível</span></div>\n" +
            "            </div>\n" +
            "            <!-- Processo principal -->\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n" +
            "<div class=\"unj-entity-header__details\">\n" +
            "    <div class=\"container\">\n" +
            "        <div class=\"unj-ta-r\">\n" +
            "            <a href=\"#maisDetalhes\" class=\"unj-link-collapse\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\"maisDetalhes\">\n" +
            "                <span class=\"unj-link-collapse__show\">\n" +
            "                    <i class=\"unj-link-collapse__icon glyph glyph-chevron-down\"></i>\n" +
            "                    Mais\n" +
            "                </span>\n" +
            "                <span class=\"unj-link-collapse__hide\">\n" +
            "                    <i class=\"unj-link-collapse__icon glyph glyph-chevron-up\"></i>\n" +
            "                    Recolher\n" +
            "                </span>\n" +
            "            </a>\n" +
            "        </div>\n" +
            "        <div id=\"maisDetalhes\" class=\"collapse\" aria-expanded=\"false\">\n" +
            "            <div class=\"row unj-row--border-top\">\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span class=\"unj-label\">Relator</span>\n" +
            "                    <div class=\"line-clamp__2\" id=\"relatorProcesso\"><span title=\"DES. LUIZ TADEU BARBOSA SILVA\">DES. LUIZ TADEU BARBOSA SILVA</span></div>\n" +
            "                </div>\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span class=\"unj-label\">Valor da ação</span>\n" +
            "                    <div class=\"line-clamp__2\" id=\"valorAcaoProcesso\"><span title=\"10.000,00\">10.000,00</span></div>\n" +
            "                </div>\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span class=\"unj-label\">Outros números</span>\n" +
            "                    <div class=\"line-clamp__2\"><br/>0821901-51.2018.8.12.0001</div>\n" +
            "                </div>\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span class=\"unj-label\">Origem</span>\n" +
            "                    <div class=\"line-clamp__2\"><span title=\"Comarca de Campo Grande / Campo Grande / 3ª Vara de Fazenda Pública e de Registros Públicos\">Comarca de Campo Grande / Campo Grande / 3ª Vara de Fazenda Pública e de Registros Públicos</span></div>\n" +
            "                </div>\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span class=\"unj-label\">Volume / Apenso</span>\n" +
            "                    <div class=\"line-clamp__2\" id=\"volumeApensoProcesso\"><span title=\"1 / 0\">1 / 0</span></div>\n" +
            "                </div>\n" +
            "                <div class=\"col-lg-3 mb-2\">\n" +
            "                    <span class=\"unj-label\">Observações </span>\n" +
            "                    <div class=\"line-clamp__2\"><span title=\"Pedido de justiça gratuita fls. 813\">Pedido de justiça gratuita fls. 813</span></div>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "\n" +
            "</div>";

    @Test()
    public void findProcessData_SECOND_LEVEL() {
        Document doc = Jsoup.parse(BASIC_DATA_SECOND_LEVEL);
        TJMSCrawler crawler = new TJMSCrawler();
        ProcessDataTO to = crawler.findProcessData(doc);

        Assertions.assertEquals("0821901-51.2018.8.12.0001", to.getNumber());
        Assertions.assertEquals("Apelação Cível", to.getClazz());
        Assertions.assertEquals("Cível", to.getArea());
        Assertions.assertEquals("Obrigação de Fazer / Não Fazer", to.getSubject());
        Assertions.assertNull(to.getDistributionDate());
        Assertions.assertNull(to.getJudge());
        Assertions.assertEquals("10.000,00", to.getAmount());
    }

    String PARTS_HTML = "<table id=\"tablePartesPrincipais\" style=\"margin-left:15px; margin-top:1px;\" align=\"center\" border=\"0\" cellPadding=\"0\" cellSpacing=\"0\" width=\"98%\">\n" +
            "    <tr class=\"fundoClaro poloAtivo\">\n" +
            "        <td valign=\"top\" width=\"141\" style=\"padding-bottom: 5px\" class=\"label\">\n" +
            "            <span class=\"mensagemExibindo tipoDeParticipacao\">Apelante:&nbsp;</span>\n" +
            "        </td>\n" +
            "        <td width=\"*\" align=\"left\" style=\"padding-bottom: 5px\" class=\"nomeParteEAdvogado\">\n" +
            "            Leidi Silva Ormond Galvão\n" +
            "            <br/><span class=\"mensagemExibindo\">Advogada:&nbsp</span>Adriana Catelan Skowronski <input type=\"hidden\" value=\"10227MS\">&nbsp;\n" +
            "            <br/><span class=\"mensagemExibindo\">Advogada:&nbsp</span>Ana Silvia Pessoa Salgado Moura <input type=\"hidden\" value=\"7317MS\">&nbsp;\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "    <tr class=\"fundoClaro poloPassivo\">\n" +
            "        <td valign=\"top\" width=\"141\" style=\"padding-bottom: 5px\" class=\"label\">\n" +
            "            <span class=\"mensagemExibindo tipoDeParticipacao\">Apelado:&nbsp;</span>\n" +
            "        </td>\n" +
            "        <td width=\"*\" align=\"left\" style=\"padding-bottom: 5px\" class=\"nomeParteEAdvogado\">\n" +
            "            Estado de Mato Grosso do Sul\n" +
            "            <br/><span class=\"mensagemExibindo\">Proc. do Estado:&nbsp</span>Nathália dos Santos Paes de Barros <input type=\"hidden\" value=\"10233MS\">&nbsp;\n" +
            "\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "</table>";

    @Test()
    public void findProcessAuthor() {
        Document doc = Jsoup.parse(PARTS_HTML);
        TJMSCrawler crawler = new TJMSCrawler();
        PartTO to = crawler.findProcessAuthor(doc);

        Assertions.assertEquals("Leidi Silva Ormond Galvão", to.getName());
        Assertions.assertNotNull(to.getLawyerList());
        Assertions.assertEquals(2, to.getLawyerList().size());
        Assertions.assertEquals("Adriana Catelan Skowronski", to.getLawyerList().get(0));
        Assertions.assertEquals("Ana Silvia Pessoa Salgado Moura", to.getLawyerList().get(1));
    }

    @Test()
    public void findProcessDefendant() {
        Document doc = Jsoup.parse(PARTS_HTML);
        TJMSCrawler crawler = new TJMSCrawler();
        PartTO to = crawler.findProcessDefendant(doc);

        Assertions.assertEquals("Estado de Mato Grosso do Sul", to.getName());
        Assertions.assertNotNull(to.getLawyerList());
        Assertions.assertEquals(1, to.getLawyerList().size());
        Assertions.assertEquals("Nathália dos Santos Paes de Barros", to.getLawyerList().get(0));
    }
}