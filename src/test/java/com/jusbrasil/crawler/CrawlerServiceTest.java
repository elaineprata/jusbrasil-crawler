package com.jusbrasil.crawler;

import com.jusbrasil.common.*;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

public class CrawlerServiceTest {

    class MyBasicCrawler extends BasicCrawler implements Crawler {

        String trCode;
        ProcessDataTO firstLevelTO;
        ProcessDataTO secondLevelTO;

        public void setTrCode(String trCode) {
            this.trCode = trCode;
        }

        public void setFirstLevelTO(ProcessDataTO firstLevelTO) {
            this.firstLevelTO = firstLevelTO;
        }

        public void setSecondLevelTO(ProcessDataTO secondLevelTO) {
            this.secondLevelTO = secondLevelTO;
        }

        @Override
        public String getTRCode() {
            return this.trCode;
        }

        @Override
        protected String buildUrl(ProcessLevel level, String number) {
            return null;
        }

        @Override
        public ProcessDataTO findProcess(ProcessLevel level, String number)
                throws IOException, ProcessNotFoundException {

            ProcessDataTO to = getTOByLevel(level);

            if (to == null) {
                throw new ProcessNotFoundException();
            } else {
                return to;
            }
        }

        private ProcessDataTO getTOByLevel(ProcessLevel level) {
            if (level == ProcessLevel.FIRST) {
                return this.firstLevelTO;
            } else {
                return this.secondLevelTO;
            }
        }

        @Override
        protected ProcessDataTO findProcessData(Document doc) {
            return null;
        }

        @Override
        protected PartTO findProcessAuthor(Document doc) {
            return null;
        }

        @Override
        protected PartTO findProcessDefendant(Document doc) {
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyBasicCrawler crawler = (MyBasicCrawler) o;
            return Objects.equals(trCode, crawler.trCode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(trCode);
        }
    }

    @Test
    public void initializeCrawlerMap() {

        MyBasicCrawler crawlerA = new MyBasicCrawler();
        crawlerA.setTrCode("A");

        MyBasicCrawler crawlerB = new MyBasicCrawler();
        crawlerB.setTrCode("B");

        MyBasicCrawler crawlerC = new MyBasicCrawler();
        crawlerC.setTrCode("C");

        CrawlerService service = new CrawlerService(null, crawlerA, crawlerB);
        service.initializeCrawlerMap(crawlerA, crawlerB, crawlerC);

        Map<String, Crawler> map = service.crawlerMap;

        Assertions.assertNotNull(map);
        Assertions.assertNotNull(map.keySet());
        Assertions.assertEquals(3, map.keySet().size());

        Assertions.assertEquals(map.get("A"), crawlerA);
        Assertions.assertEquals(map.get("B"), crawlerB);
        Assertions.assertEquals(map.get("C"), crawlerC);
    }

    @Test
    public void findCrawler() throws Exception {
        MyBasicCrawler crawlerA = new MyBasicCrawler();
        crawlerA.setTrCode("A");

        MyBasicCrawler crawlerB = new MyBasicCrawler();
        crawlerB.setTrCode("B");

        MyBasicCrawler crawlerC = new MyBasicCrawler();
        crawlerC.setTrCode("C");

        CrawlerService service = new CrawlerService(null, crawlerA, crawlerB);
        service.initializeCrawlerMap(crawlerA, crawlerB, crawlerC);

        Assertions.assertEquals(crawlerA, service.findCrawler("A"));
        Assertions.assertEquals(crawlerB, service.findCrawler("B"));

        Assertions.assertThrows(CrowlerNotFoundException.class, () -> {
            service.findCrawler("ABC");
        });
    }

    @Test
    public void findProcess() throws Exception {

        ProcessTO processTO = new ProcessTOBuilder().build();

        MyBasicCrawler crawlerA = new MyBasicCrawler();
        crawlerA.setTrCode("01");
        crawlerA.firstLevelTO = processTO.getFirstLevelData();
        crawlerA.secondLevelTO = processTO.getSecondLevelData();

        MyBasicCrawler crawlerB = new MyBasicCrawler();
        crawlerB.setTrCode("02");
        crawlerB.firstLevelTO = processTO.getFirstLevelData();
        crawlerB.secondLevelTO = null;

        MyBasicCrawler crawlerC = new MyBasicCrawler();
        crawlerC.setTrCode("03");
        crawlerC.firstLevelTO = null;
        crawlerC.secondLevelTO = null;

        ProcessNumberValidator validator = new ProcessNumberValidator();
        CrawlerService service = new CrawlerService(validator, crawlerA, crawlerB);

        ProcessTO to01 = service.findProcess("0710802-55.2018.8.01.0001");
        Assertions.assertEquals("processNumber", to01.getFirstLevelData().getNumber());
        Assertions.assertEquals("processNumber2", to01.getSecondLevelData().getNumber());

        ProcessTO to02 = service.findProcess("0710802-55.2018.8.02.0001");
        Assertions.assertEquals("processNumber", to02.getFirstLevelData().getNumber());
        Assertions.assertNull(to02.getSecondLevelData());

        CrawlerService service2 = new CrawlerService(validator, crawlerA, crawlerC);
        Assertions.assertThrows(ProcessNotFoundException.class, () -> {
            service2.findProcess("0710802-55.2018.8.03.0001");
        });
    }
}
