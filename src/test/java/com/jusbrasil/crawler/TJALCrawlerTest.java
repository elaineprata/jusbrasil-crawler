package com.jusbrasil.crawler;

import com.jusbrasil.common.PartTO;
import com.jusbrasil.common.ProcessDataTO;
import com.jusbrasil.common.ProcessNotFoundException;
import com.jusbrasil.common.ProcessNumberValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TJALCrawlerTest {

    @Test()
    public void findProcessData() {
        String table = "<div>" +
                "<table id=\"\" class=\"secaoFormBody\" width=\"100%\" style=\"\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\"></td>\n" +
                "    </tr>\n" +
                "</table>\n" +
                "<table id=\"\" class=\"secaoFormBody\" width=\"100%\" style=\"\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"dadosFmt.numero\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Processo:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\">\n" +
                "            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"\">\n" +
                "                <tr>\n" +
                "                    <td><!-- Atributos -->\n" +
                "                        <span class=\"\">0710802-55.2018.8.02.0001</span>\n" +
                "                        <span class=\"\"></span>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"numeroProcessoSeDependente\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Classe:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\">\n" +
                "            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"\">\n" +
                "                <tr>\n" +
                "                    <td>\n" +
                "                        <span id=\"\"><span id=\"\" class=\"\">Procedimento Comum Cível</span></span>&nbsp;\n" +
                "                        <span id=\"\"><span id=\"\" class=\"\">&nbsp;</span></span>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\"></td>\n" +
                "        <td valign=\"\">\n" +
                "            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"\">\n" +
                "                <tr>\n" +
                "                    <td><span class=\"labelClass\">Área:</span> Cível</td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Assunto:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\">\n" +
                "            <span id=\"\" class=\"\">Dano Material</span>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Outros assuntos:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\">\n" +
                "            <span id=\"\" class=\"\">Dano Moral</span>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Distribuição:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\"><span id=\"\" class=\"\">02/05/2018 às 19:01 - Sorteio</span></td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\"></td>\n" +
                "        <td valign=\"\"><span id=\"\" class=\"\">4ª Vara Cível da Capital - Foro de Maceió</span></td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Controle:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\"><span id=\"\" class=\"\">2018/000520</span></td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Juiz:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\"><span id=\"\" class=\"\">José Cícero Alves da Silva</span></td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Valor da ação:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\"><span id=\"\" class=\"\">R$         281.178,42</span></td>\n" +
                "    </tr>\n" +
                "    <tr class=\"\">\n" +
                "        <td id=\"\" width=\"150\" valign=\"\">\n" +
                "            <label for=\"tag.dados.rotulo.custas\" class=\"labelClass\" style=\"text-align:right;font-weight:bold;;\">Custas:</label>\n" +
                "        </td>\n" +
                "        <td valign=\"\">\n" +
                "            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"\">\n" +
                "                <tr>\n" +
                "                    <td>\n" +
                "                        <a href=\"https://www2.tjal.jus.br/ccpweb/abrirConsultaCustas.do?cdProcesso=01000O7550000&nuProcesso=0710802-55.2018.8.02.0001\" target=\"_blank\">Visualizar custas</a>\n" +
                "\n" +
                "                        <span style=\"color: red; font-weight: bold;\">(há custas pendentes)</span>\n" +
                "\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "</table>\n";

        Document doc = Jsoup.parse(table);
        TJALCrawler crawler = new TJALCrawler(new ProcessNumberValidator());
        ProcessDataTO to = crawler.findProcessData(doc);

        Assertions.assertEquals("0710802-55.2018.8.02.0001", to.getNumber());
        Assertions.assertEquals("Procedimento Comum Cível", to.getClazz());
        Assertions.assertEquals("Cível", to.getArea());
        Assertions.assertEquals("Dano Material", to.getSubject());
        Assertions.assertEquals("02/05/2018 às 19:01 - Sorteio", to.getDistributionDate());
        Assertions.assertEquals("José Cícero Alves da Silva", to.getJudge());
        Assertions.assertEquals("R$ 281.178,42", to.getAmount());
    }


    String PARTS_HTML = "<table id=\"tablePartesPrincipais\" style=\"margin-left:15px; margin-top:1px;\" align=\"center\" border=\"0\" cellPadding=\"0\" cellSpacing=\"0\" width=\"98%\">\n" +
            "    <tr class=\"fundoClaro\">\n" +
            "        <td valign=\"top\" align=\"right\" width=\"141\" style=\"padding-bottom: 5px\">\n" +
            "            <span class=\"mensagemExibindo\">Autor:&nbsp;</span>\n" +
            "        </td>\n" +
            "        <td width=\"*\" align=\"left\" style=\"padding-bottom: 5px\">\n" +
            "            José Carlos Cerqueira Souza Filho<br />\n" +
            "            <span class=\"mensagemExibindo\">Advogado:&nbsp;</span> Vinicius Faria de Cerqueira&nbsp;\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "    <tr class=\"fundoClaro\">\n" +
            "        <td valign=\"top\" align=\"right\" width=\"141\" style=\"padding-bottom: 5px\">\n" +
            "            <span class=\"mensagemExibindo\">Ré:&nbsp;</span>\n" +
            "        </td>\n" +
            "        <td width=\"*\" align=\"left\" style=\"padding-bottom: 5px\">\n" +
            "            Cony Engenharia Ltda.\n" +
            "            <br />\n" +
            "            <span class=\"mensagemExibindo\">Advogado:&nbsp;</span> Marcus Vinicius Cavalcante Lins Filho&nbsp;\n" +
            "            <br />\n" +
            "            <span class=\"mensagemExibindo\">Advogado:&nbsp;</span> Thiago Maia Nobre Rocha&nbsp;\n" +
            "            <br />\n" +
            "            <span class=\"mensagemExibindo\">Advogado:&nbsp;</span> Orlando de Moura Cavalcante Neto&nbsp;\n" +
            "        </td>\n" +
            "    </tr>\n" +
            "</table>";

    @Test()
    public void findProcessAuthor() {
        Document doc = Jsoup.parse(PARTS_HTML);
        TJALCrawler crawler = new TJALCrawler(new ProcessNumberValidator());
        PartTO to = crawler.findProcessAuthor(doc);

        Assertions.assertEquals("José Carlos Cerqueira Souza Filho", to.getName());
        Assertions.assertNotNull(to.getLawyerList());
        Assertions.assertEquals(1, to.getLawyerList().size());
        Assertions.assertEquals("Vinicius Faria de Cerqueira", to.getLawyerList().get(0));
    }

    @Test()
    public void findProcessDefendant() {
        Document doc = Jsoup.parse(PARTS_HTML);
        TJALCrawler crawler = new TJALCrawler(new ProcessNumberValidator());
        PartTO to = crawler.findProcessDefendant(doc);

        Assertions.assertEquals("Cony Engenharia Ltda.", to.getName());
        Assertions.assertNotNull(to.getLawyerList());
        Assertions.assertEquals(3, to.getLawyerList().size());
        Assertions.assertEquals("Marcus Vinicius Cavalcante Lins Filho", to.getLawyerList().get(0));
        Assertions.assertEquals("Thiago Maia Nobre Rocha", to.getLawyerList().get(1));
        Assertions.assertEquals("Orlando de Moura Cavalcante Neto", to.getLawyerList().get(2));
    }
}