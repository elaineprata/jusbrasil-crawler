package com.jusbrasil.user;

public class UserConfigBuilder {
    private long id = 987;

    private String name = "Ana Maria";

    private String webhookUrl = "http://anamaria.com.br/webhook";


    public UserConfigBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public UserConfigBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserConfigBuilder withWebhookUrl(String url) {
        this.webhookUrl = url;
        return this;
    }

    public UserConfig build() {
        return new UserConfig(id, name, webhookUrl);
    }
}
