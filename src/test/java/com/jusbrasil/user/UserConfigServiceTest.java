package com.jusbrasil.user;

import com.jusbrasil.common.UserConfigNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class UserConfigServiceTest {

    @Mock
    private UserConfigRepository userConfigRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test()
    public void findUserConfig() throws Exception {

        UserConfig userConfig = new UserConfigBuilder().build();

        when(userConfigRepository.findById(any())).thenReturn(Optional.of(userConfig));

        UserConfigService service = new UserConfigService(userConfigRepository);
        UserConfig found = service.findUserConfig(123);

        Assertions.assertEquals(userConfig.getId(), found.getId());
        Assertions.assertEquals(userConfig.getName(), found.getName());
        Assertions.assertEquals(userConfig.getWebhookUrl(), found.getWebhookUrl());
    }

    @Test()
    public void findUserConfig_notFound() {

        when(userConfigRepository.findById(any())).thenReturn(Optional.empty());

        UserConfigService service = new UserConfigService(userConfigRepository);

        Assertions.assertThrows(UserConfigNotFoundException.class,
                () ->  service.findUserConfig(123));
    }

    @Test()
    public void createUserConfig() {
        UserConfig userConfig = new UserConfigBuilder().build();

        when(userConfigRepository.save(any())).thenReturn(userConfig);

        UserConfigService service = new UserConfigService(userConfigRepository);
        service.createUserConfig(userConfig);

        verify(userConfigRepository, times(1)).save(any());
    }
}