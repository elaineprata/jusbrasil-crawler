package com.jusbrasil.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProcessNumberValidatorTest {

    @Test()
    public void validateProcessNumber() throws Exception {
        String processNumber = "0710802-55.2018.8.02.0001";

        ProcessNumberValidator validator = new ProcessNumberValidator();
        validator.validateProcessNumber(processNumber);
    }

    @Test()
    public void validateProcessNumber_InvalidProcessNumberFormatException() throws Exception {
        String processNumberA = "0710802-55.2018.A.02.0001";
        String processNumberB = "07108025520181020001";
        String processNumberC = "sfkshkdfhsdkj";

        ProcessNumberValidator validator = new ProcessNumberValidator();

        Assertions.assertThrows(InvalidProcessNumberFormatException.class, () -> {
            validator.validateProcessNumber(processNumberA);
        });

        Assertions.assertThrows(InvalidProcessNumberFormatException.class, () -> {
            validator.validateProcessNumber(processNumberB);
        });

        Assertions.assertThrows(InvalidProcessNumberFormatException.class, () -> {
            validator.validateProcessNumber(processNumberC);
        });
    }

    @Test()
    public void findTRCode() throws Exception {
        String processNumberA = "0710802-55.2018.8.02.0001";
        String processNumberB = "0710802-55.2018.8.12.0001";

        ProcessNumberValidator validator = new ProcessNumberValidator();

        Assertions.assertEquals("02", validator.findTRCode(processNumberA));
        Assertions.assertEquals("12", validator.findTRCode(processNumberB));
    }

    @Test()
    public void findProcessNumberAndYear() throws Exception {
        String processNumberA = "0710802-55.2018.8.02.0001";
        String processNumberB = "0710802-56.2020.8.12.0001";

        ProcessNumberValidator validator = new ProcessNumberValidator();

        Assertions.assertEquals("0710802-55.2018", validator.findProcessNumberAndYear(processNumberA));
        Assertions.assertEquals("0710802-56.2020", validator.findProcessNumberAndYear(processNumberB));
    }

    @Test()
    public void findForoNumber() throws Exception {
        String processNumberA = "0710802-55.2018.12.02.0001";
        String processNumberB = "0710802-56.2020.23.12.0008";

        ProcessNumberValidator validator = new ProcessNumberValidator();

        Assertions.assertEquals("0001", validator.findForoNumber(processNumberA));
        Assertions.assertEquals("0008", validator.findForoNumber(processNumberB));
    }
}