package com.jusbrasil.common;

public class ProcessTOBuilder {
    private ProcessDataTO firstLevelData = new ProcessDataTO("processNumber", "class", "area", "subject", "distributionDate", "judge", "amount");
    private ProcessDataTO secondLevelData = new ProcessDataTO("processNumber2", "class2", "area2", "subject2", "distributionDate2", "judge2", "amount2");

    public ProcessTO build() {
        return new ProcessTO(firstLevelData, secondLevelData);
    }
}
