package com.jusbrasil.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jusbrasil.process.ProcessSearchRequestTO;
import com.jusbrasil.process.ProcessSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageReceiver {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProcessSearchService searchService;

    @JmsListener(destination = "process-search-queue")
    public void receiveQueue(String text) throws Exception {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Message Received: " + text);

        ProcessSearchRequestTO to = objectMapper.readValue(text, ProcessSearchRequestTO.class);

        // TODO call service to handle the search
        searchService.findProcess(to);

        System.out.println("TO received: " + to);
    }
}