package com.jusbrasil.message;

import javax.jms.Queue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jusbrasil.process.ProcessSearchRequestTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Queue queue;

    @Autowired
    private ObjectMapper objectMapper;

    public void sendMessage(ProcessSearchRequestTO request) throws Exception {
        this.jmsMessagingTemplate.convertAndSend(this.queue, objectMapper.writeValueAsString(request));
    }
}