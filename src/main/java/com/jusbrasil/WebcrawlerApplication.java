package com.jusbrasil;

import com.jusbrasil.user.UserConfig;
import com.jusbrasil.user.UserConfigRepository;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.jms.Queue;
import java.util.Optional;

@SpringBootApplication
@EnableJms
@EnableSwagger2
public class WebcrawlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebcrawlerApplication.class, args);
	}

	@Bean
	CommandLineRunner init(UserConfigRepository userConfigRepository) {
		userConfigRepository.deleteAll();
		return args -> {
			Optional<UserConfig> obj = userConfigRepository.findById(123L);
			if (!obj.isPresent()) {
				userConfigRepository.save(new UserConfig(123, "Elaine Prata", "https://jusbrasil-webhook.free.beeceptor.com"));
			}
		};
	}

	@Bean
	public Queue queue() {
		return new ActiveMQQueue("process-search-queue");
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build();
	}
}