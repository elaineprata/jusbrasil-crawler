package com.jusbrasil.user;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserConfigRepository extends MongoRepository<UserConfig, Long> {

}
