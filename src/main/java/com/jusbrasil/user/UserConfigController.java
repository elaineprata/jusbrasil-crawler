package com.jusbrasil.user;

import com.jusbrasil.common.ProcessTO;
import com.jusbrasil.process.ProcessSearchRequestTO;
import com.jusbrasil.process.ProcessSearchUserRequestTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/jusbrasil/user-configs")
public class UserConfigController {
    UserConfigService userConfigService;

    @Autowired
    UserConfigController(UserConfigService userConfigService) {
        this.userConfigService = userConfigService;
    }

    @GetMapping(path="/{number}")
    public ResponseEntity<UserConfig> getUserConfig(@PathVariable("number") long number) throws Exception {
        return ResponseEntity.ok(this.userConfigService.findUserConfig(number));
    }

    @GetMapping()
    public ResponseEntity<List<UserConfig>> listUserConfig() throws Exception {
        return ResponseEntity.ok(this.userConfigService.listAll());
    }

    @PostMapping()
    public ResponseEntity<?> createUserConfig(
            @Valid
            @RequestBody
            UserConfig userConfig) throws Exception {
        UserConfig created = this.userConfigService.createUserConfig(userConfig);
        return ResponseEntity.created(new URI("/jusbrasil/user-configs/" + created.getId())).body(created);
    }
}