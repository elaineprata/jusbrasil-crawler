package com.jusbrasil.user;

import com.jusbrasil.common.UserConfigNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserConfigService {

    UserConfigRepository userConfigRepository;

    public UserConfigService() {
    }

    @Autowired
    public UserConfigService(UserConfigRepository userConfigRepository) {
        this.userConfigRepository = userConfigRepository;
    }

    public UserConfig findUserConfig(long userId) throws Exception {
        Optional<UserConfig> optUserConfig =  userConfigRepository.findById(userId);

        if (!optUserConfig.isPresent()) {
            throw new UserConfigNotFoundException();
        }

        return optUserConfig.get();
    }

    public UserConfig createUserConfig(UserConfig userConfig) {
        return this.userConfigRepository.save(userConfig);
    }

    public List<UserConfig> listAll() {
        return this.userConfigRepository.findAll();
    }
}
