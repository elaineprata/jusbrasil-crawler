package com.jusbrasil.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user-config")
public class UserConfig {

    @Id
    private long id;

    private String name;

    private String webhookUrl;

    public UserConfig() {

    }

    public UserConfig(long id, String name, String webhookUrl) {
        this.id = id;
        this.name = name;
        this.webhookUrl = webhookUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebhookUrl() {
        return webhookUrl;
    }

    public void setWebhookUrl(String webhookUrl) {
        this.webhookUrl = webhookUrl;
    }
}
