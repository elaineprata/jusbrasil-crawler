package com.jusbrasil.crawler;

import com.jusbrasil.common.MovementTO;
import com.jusbrasil.common.PartTO;
import com.jusbrasil.common.ProcessDataTO;
import com.jusbrasil.common.ProcessNotFoundException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class BasicCrawler {

    public abstract String getTRCode();

    protected Document connect(String url) throws IOException {

        System.out.println("\n\n\nURL: " + url);

        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public ProcessDataTO findProcess(ProcessLevel level, String number) throws IOException, ProcessNotFoundException {

        Document doc = connect(buildUrl(level, number));

        validateSearchResult(doc);

        ProcessDataTO processDataTO = findProcessData(doc);
        PartTO author = findProcessAuthor(doc);
        PartTO defendant = findProcessDefendant(doc);
        List<MovementTO> movementList = findProcessMovements(doc);

        processDataTO.setAuthor(author);
        processDataTO.setDefendant(defendant);
        processDataTO.setMovementList(movementList);

        return processDataTO;
    }

    protected void validateSearchResult(Document doc) throws ProcessNotFoundException {
        Element element = doc.getElementById("mensagemRetorno");
        if ((element != null) && element.html().contains("Não existem informações disponíveis")) {
            throw new ProcessNotFoundException();
        }
    }

    protected abstract String buildUrl(ProcessLevel level, String number);
    protected abstract ProcessDataTO findProcessData(Document doc);
    protected abstract PartTO findProcessAuthor(Document doc);
    protected abstract PartTO findProcessDefendant(Document doc);

    protected List<MovementTO> findProcessMovements(Document doc) {
        List<MovementTO> movementList = new ArrayList<>();

        Element element = doc.getElementById("tabelaTodasMovimentacoes");

        Elements trs = element.select("tr");

        for (Element tr: trs) {
            Elements tds = tr.select("td");
            movementList.add(new MovementTO(tds.get(0).text(), tds.get(2).text()));
        }

        return movementList;
    }

    protected PartTO buildParty(String partyString) {
        List<String> lawyerList = new ArrayList<>();
        String[] lawyers = partyString.split("(Advogad[oa]|RepreLeg|Proc. do Estado):");
        for (int i = 1; i < lawyers.length; i++) {
            lawyerList.add(htmlTrim(lawyers[i]));
        }

        return new PartTO(lawyers[0].trim(), lawyerList);
    }

    protected String htmlTrim(String text) {
        return text.replaceAll("\\u00A0", "").trim();
    }

    protected String getOptionalElementText(Document doc, String id) {
        Element element = doc.getElementById(id);
        if (element != null) {
            return element.text();
        } else {
            return null;
        }
    }
}
