package com.jusbrasil.crawler;

import com.jusbrasil.common.PartTO;
import com.jusbrasil.common.ProcessDataTO;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("tjmsCrawler")
public class TJMSCrawler extends BasicCrawler implements Crawler {

    @Value("${crawler.tjms.code}")
    private String trCode;

    @Value("${crawler.tjms.url.first-level}")
    private String firstLevelUrl;

    @Value("${crawler.tjms.url.second-level}")
    private String secondLevelUrl;

    @Override
    public String getTRCode() {
        return trCode;
    }

    @Override
    protected String buildUrl(ProcessLevel level, String number) {
        switch (level) {
            case SECOND:
                return secondLevelUrl.replace("[[PROCESS_NUMBER]]", number);
            default:
                return firstLevelUrl.replace("[[PROCESS_NUMBER]]", number);
        }
    }

    @Override
    protected ProcessDataTO findProcessData(Document doc) {

        String processNumber = doc.getElementById("numeroProcesso").text();
        String clazz = doc.getElementById("classeProcesso").text();
        String area = doc.getElementById("areaProcesso").text();
        String subject = doc.getElementById("assuntoProcesso").text();
        String judge = getOptionalElementText(doc, "juizProcesso");
        String distributionDate = getOptionalElementText(doc, "dataHoraDistribuicaoProcesso");
        String amount = doc.getElementById("valorAcaoProcesso").text();

        return new ProcessDataTO(processNumber, clazz, area, subject, distributionDate, judge, amount);
    }

    @Override
    protected PartTO findProcessAuthor(Document doc) {
        String partyString = doc.select("td[class=nomeParteEAdvogado]").first().text();
        return buildParty(partyString);
    }

    @Override
    protected PartTO findProcessDefendant(Document doc) {
        String partyString = doc.select("td[class=nomeParteEAdvogado]").get(1).text();
        return buildParty(partyString);
    }
}
