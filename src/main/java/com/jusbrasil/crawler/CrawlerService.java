package com.jusbrasil.crawler;

import com.jusbrasil.common.ProcessDataTO;
import com.jusbrasil.common.ProcessNumberValidator;
import com.jusbrasil.common.ProcessTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CrawlerService {

    ProcessNumberValidator validator;
    Crawler tjalCrawler;
    Crawler tjmsCrawler;

    Map<String, Crawler> crawlerMap = new HashMap<>();

    @Autowired
    CrawlerService(ProcessNumberValidator validator, Crawler tjalCrawler, Crawler tjmsCrawler) {
        this.validator = validator;
        this.tjalCrawler = tjalCrawler;
        this.tjmsCrawler = tjmsCrawler;

        initializeCrawlerMap(tjalCrawler, tjmsCrawler);
    }

    public ProcessTO findProcess(String number) throws Exception {

        validator.validateProcessNumber(number);

        String trCode = validator.findTRCode(number);

        Crawler crawler = findCrawler(trCode);

        ProcessDataTO firstLevelProcessData = crawler.findProcess(ProcessLevel.FIRST, number);
        ProcessDataTO secondLevelProcessData = null;
        try {
            secondLevelProcessData = crawler.findProcess(ProcessLevel.SECOND, number);
        // TODO especific exception
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ProcessTO(firstLevelProcessData, secondLevelProcessData);
    }

    protected void initializeCrawlerMap(Crawler...crawlers) {
        crawlerMap = new HashMap<>();

        for (Crawler crawler: crawlers) {
            crawlerMap.put(crawler.getTRCode(), crawler);
        }
    }

    protected Crawler findCrawler(String trCode) throws CrowlerNotFoundException {
        Crawler crawler = crawlerMap.get(trCode);
        if (crawler == null) {
            throw new CrowlerNotFoundException();
        }
        return crawler;
    }
}
