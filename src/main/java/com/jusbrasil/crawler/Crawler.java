package com.jusbrasil.crawler;

import com.jusbrasil.common.ProcessDataTO;

public interface Crawler {

    public String getTRCode();
    public ProcessDataTO findProcess(ProcessLevel level, String number) throws Exception;
}
