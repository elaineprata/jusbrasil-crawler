package com.jusbrasil.crawler;

import com.jusbrasil.common.PartTO;
import com.jusbrasil.common.ProcessDataTO;
import com.jusbrasil.common.ProcessNotFoundException;
import com.jusbrasil.common.ProcessNumberValidator;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("tjalCrawler")
public class TJALCrawler extends BasicCrawler implements Crawler {

    @Value("${crawler.tjal.code}")
    private String trCode;

    @Value("${crawler.tjal.url.first-level}")
    private String firstLevelUrl;

    @Value("${crawler.tjal.url.second-level}")
    private String secondLevelUrl;

    private ProcessNumberValidator validator;

    @Autowired
    public TJALCrawler(ProcessNumberValidator validator) {
        this.validator = validator;
    }

    @Override
    public String getTRCode() {
        return trCode;
    }

    @Override
    protected String buildUrl(ProcessLevel level, String number) {

        switch (level) {
            case SECOND:
                return secondLevelUrl
                        .replace("[[PROCESS_NUMBER_AND_YEAR]]", validator.findProcessNumberAndYear(number))
                        .replace("[[FORO_NUMBER]]", validator.findForoNumber(number))
                        .replace("[[PROCESS_NUMBER]]", number);
            default:
                return firstLevelUrl.replace("[[PROCESS_NUMBER]]", number);
        }
    }

    @Override
    protected ProcessDataTO findProcessData(Document doc) {

        Elements tables = doc.select("table[class=secaoFormBody]");
        Element table = tables.get(1);
        Elements trs = table.select("tr");

        // process number
        String process = trs.get(0).select("td").get(1).select("span").first().html();

        // clazz
        Element span = trs.get(2).select("td").get(2).select("span").get(1);
        String clazz = span.html();

        Element tdArea = trs.get(5).select("td").first();
        String area = tdArea.text().split(": ")[1];

        Element tdSubject = trs.get(6).select("td").get(1).select("span").first();
        String subject = tdSubject.html();

        Element tdDate = trs.get(8).select("td").get(1).select("span").first();
        String date = tdDate.html();

        Element tdJudge = trs.get(11).select("td").get(1).select("span").first();
        String judge = tdJudge.html();

        Element tdAmount = trs.get(12).select("td").get(1).select("span").first();
        String amount = tdAmount.html();

        return new ProcessDataTO(process, clazz, area, subject, date, judge, amount);
    }

    @Override
    protected PartTO findProcessAuthor(Document doc) {

        Element element = doc.getElementById("tablePartesPrincipais");
        String author = element.select("tr").get(0).select("td").get(1).text();

        return buildParty(author);
    }

    @Override
    protected PartTO findProcessDefendant(Document doc) {

        Element element = doc.getElementById("tablePartesPrincipais");
        String other = element.select("tr").get(1).select("td").get(1).text();

        return buildParty(other);
    }
}