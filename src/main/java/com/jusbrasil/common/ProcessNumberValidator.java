package com.jusbrasil.common;

import org.springframework.stereotype.Component;

@Component
public class ProcessNumberValidator {

    public void validateProcessNumber(String number) throws InvalidProcessNumberFormatException {
        if (!number.matches("\\d{7}-\\d{2}\\.\\d{4}\\.\\d\\.\\d{2}\\.\\d{4}")) {
            throw new InvalidProcessNumberFormatException();
        }
    }

    public String findTRCode(String processNumber) {
        String[] numberParts = processNumber.split("\\.");
        return numberParts[3];
    }

    public String findProcessNumberAndYear(String processNumber) {
        String[] numberParts = processNumber.split("\\.");
        return numberParts[0] + "." + numberParts[1];
    }

    public String findForoNumber(String processNumber) {
        String[] numberParts = processNumber.split("\\.");
        return numberParts[4];
    }
}
