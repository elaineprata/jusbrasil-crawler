package com.jusbrasil.common;

import java.util.ArrayList;
import java.util.List;

public class PartTO {
    String name;
    List<String> lawyerList = new ArrayList<>();

    public PartTO(String name, List<String> lawyerList) {
        this.name = name;
        this.lawyerList = lawyerList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLawyerList() {
        return lawyerList;
    }

    public void setLawyerList(List<String> lawyerList) {
        this.lawyerList = lawyerList;
    }
}
