package com.jusbrasil.common;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessTO {
    ProcessDataTO firstLevelData;
    ProcessDataTO secondLevelData;

    public ProcessTO(ProcessDataTO firstLevelData, ProcessDataTO secondLevelData) {
        this.firstLevelData = firstLevelData;
        this.secondLevelData = secondLevelData;
    }

    public ProcessDataTO getFirstLevelData() {
        return firstLevelData;
    }

    public void setFirstLevelData(ProcessDataTO firstLevelData) {
        this.firstLevelData = firstLevelData;
    }

    public ProcessDataTO getSecondLevelData() {
        return secondLevelData;
    }

    public void setSecondLevelData(ProcessDataTO secondLevelData) {
        this.secondLevelData = secondLevelData;
    }
}
