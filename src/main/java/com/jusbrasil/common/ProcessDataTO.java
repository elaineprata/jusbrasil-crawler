package com.jusbrasil.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessDataTO {
    String number;
    String clazz;
    String area;
    String subject;
    String distributionDate;
    String judge;
    String amount;
    List movementList = new ArrayList<MovementTO>();
    PartTO author;
    PartTO defendant;

    public ProcessDataTO(String processNumber, String clazz, String area, String subject, String distributionDate, String judge, String amount) {
        this.number = processNumber;
        this.clazz = clazz;
        this.area = area;
        this.subject = subject;
        this.distributionDate = distributionDate;
        this.judge = judge;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }

    public String getJudge() {
        return judge;
    }

    public void setJudge(String judge) {
        this.judge = judge;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void addMovement(String date, String description) {
        this.movementList.add(new MovementTO(date, description));
    }

    public List<MovementTO> getMovementList() {
        return movementList;
    }

    public void setMovementList(List<MovementTO> movementList) {
        this.movementList = movementList;
    }

    public PartTO getAuthor() {
        return author;
    }

    public void setAuthor(PartTO author) {
        this.author = author;
    }

    public PartTO getDefendant() {
        return defendant;
    }

    public void setDefendant(PartTO defendant) {
        this.defendant = defendant;
    }
}

