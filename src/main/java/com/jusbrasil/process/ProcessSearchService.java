package com.jusbrasil.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jusbrasil.common.ProcessTO;
import com.jusbrasil.crawler.CrawlerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessSearchService {

    private CrawlerService crawler;

    private ProcessSearchResultSender resultSender;

    private ObjectMapper objectMapper;

    Logger logger = LoggerFactory.getLogger(ProcessSearchService.class);

    @Autowired
    ProcessSearchService(CrawlerService crawler, ProcessSearchResultSender resultSender, ObjectMapper objectMapper) {
        this.crawler = crawler;
        this.resultSender = resultSender;
        this.objectMapper = objectMapper;
    }

    // called by the crawler controller
    public ProcessTO findByNumber(String number) throws Exception {
        return this.crawler.findProcess(number);
    }

    public void findProcess(ProcessSearchRequestTO searchRequest) throws Exception {
        ProcessTO processTO = this.crawler.findProcess(searchRequest.processNumber);

        logger.info("Resultado da busca para o processo {}: {}", searchRequest.processNumber, objectMapper.writeValueAsString(processTO));

        resultSender.sendSearchResult(searchRequest, processTO);
    }
}
