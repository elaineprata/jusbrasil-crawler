package com.jusbrasil.process;

import javax.validation.constraints.NotBlank;

public class ProcessSearchUserRequestTO {

    @NotBlank(message = "Número do processo é obrigatório")
    String processNumber;

    public ProcessSearchUserRequestTO() {

    }

    public ProcessSearchUserRequestTO(String processNumber) {
        this.processNumber = processNumber;
    }

    public String getProcessNumber() {
        return processNumber;
    }

    public void setProcessNumber(String processNumber) {
        this.processNumber = processNumber;
    }

}
