package com.jusbrasil.process;

import com.jusbrasil.message.MessageSender;
import com.jusbrasil.user.UserConfig;
import com.jusbrasil.user.UserConfigService;
import org.springframework.stereotype.Service;

@Service
public class ProcessSearchRequestService {

    private MessageSender messageSender;
    private UserConfigService userConfigService;

    ProcessSearchRequestService(UserConfigService userConfigService, MessageSender messageSender) {
        this.userConfigService = userConfigService;
        this.messageSender = messageSender;
    }

    public void createProcessSearchRequest(ProcessSearchRequestTO request) throws Exception {
        UserConfig userConfig = userConfigService.findUserConfig(request.userId);
        request.setResultUrl(userConfig.getWebhookUrl());

        // send message to queue
        messageSender.sendMessage(request);
    }
}
