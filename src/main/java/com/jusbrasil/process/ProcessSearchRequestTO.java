package com.jusbrasil.process;

public class ProcessSearchRequestTO extends ProcessSearchUserRequestTO {

    long userId;
    String resultUrl;

    ProcessSearchRequestTO() {
    }

    ProcessSearchRequestTO(ProcessSearchUserRequestTO userRequestTO, long userId) {
        this.processNumber = userRequestTO.processNumber;
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }
}
