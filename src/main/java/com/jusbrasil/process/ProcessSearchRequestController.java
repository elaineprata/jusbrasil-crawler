package com.jusbrasil.process;

import com.jusbrasil.common.ProcessTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

// TODO refactoring packages

@RestController
@RequestMapping("/jusbrasil")
public class ProcessSearchRequestController {

    ProcessSearchService searchService;
    ProcessSearchRequestService searchRequestService;

    ProcessSearchRequestController(
                    @Autowired
                    ProcessSearchService searchService,
                    @Autowired
                    ProcessSearchRequestService searchRequestService) {
        this.searchService = searchService;
        this.searchRequestService = searchRequestService;
    }

    @GetMapping(path="/processes/{number}")
    public ResponseEntity<ProcessTO> getMessage(@PathVariable("number") String number) throws Exception {
        return ResponseEntity.ok(this.searchService.findByNumber(number));
    }

    @PostMapping(path="/process-search")
    public ResponseEntity<?> createSearchRequest(
                                    @RequestHeader("JB_USER_ID")
                                    long userId,
                                    @Valid
                                    @RequestBody
                                    ProcessSearchUserRequestTO userSearchRequest) throws Exception {

        ProcessSearchRequestTO request = new ProcessSearchRequestTO(userSearchRequest, userId);
        this.searchRequestService.createProcessSearchRequest(request);
        return ResponseEntity.accepted().body(userSearchRequest);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }
}
