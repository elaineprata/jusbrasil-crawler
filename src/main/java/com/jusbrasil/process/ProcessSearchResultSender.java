package com.jusbrasil.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jusbrasil.common.ProcessTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ProcessSearchResultSender {

    private ObjectMapper objectMapper;

    ProcessSearchResultSender(@Autowired ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void sendSearchResult(ProcessSearchRequestTO searchRequestTO, ProcessTO processTO) throws Exception {
        RestTemplate restTemplate = getRestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(processTO), headers);

        ResponseEntity<String> response = restTemplate.postForEntity(searchRequestTO.resultUrl, request, String.class);
        // response.getBody()
    }

    protected RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
