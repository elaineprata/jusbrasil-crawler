<p align="center">
  <a href="https://www.jusbrasil.com.br" target="blank"><img src="https://static.jusbr.com/deadpool/home/default/image/bg-isometric.png" width="800" alt="JusBrasil Logo" /></a>
</p>
<p align="center">Solução de <a href="https://gitlab.com/elaineprata" target="blank">Elaine Prata</a> para o <a href="https://gist.github.com/tarsisazevedo/966d469e8a80741334d3c4dce66cbea5" target="blank">desafio</a> da <a href="https://www.jusbrasil.com.br" target="blank">JusBrasil</a>.
    <p align="center">
    
## Configuração

No arquivo application.properties é possível encontrar as URLs (de primeira e segunda instância) das buscas dos tribunais TJAL e TJMS assim como seus códigos.

```

# TJAL
crawler.tjal.code=02

crawler.tjal.url.first-level=https://www2.tjal.jus.br/cpopg/search.do?dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=SAJ&dadosConsulta.valorConsulta=[[PROCESS_NUMBER]]
crawler.tjal.url.second-level=https://www2.tjal.jus.br/cposg5/search.do?cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=[[PROCESS_NUMBER_AND_YEAR]]&foroNumeroUnificado=[[FORO_NUMBER]]&dePesquisaNuUnificado=[[PROCESS_NUMBER]]


# TJMS
crawler.tjms.code=12

crawler.tjms.url.first-level=https://esaj.tjms.jus.br/cpopg5/search.do?cbPesquisa=NUMPROC&dadosConsulta.valorConsultaNuUnificado=[[PROCESS_NUMBER]]&dadosConsulta.valorConsultaNuUnificado=UNIFICADO&dadosConsulta.tipoNuProcesso=UNIFICADO
crawler.tjms.url.second-level=https://esaj.tjms.jus.br/cposg5/search.do?cbPesquisa=NUMPROC&dePesquisaNuUnificado=[[PROCESS_NUMBER]]&dePesquisaNuUnificado=UNIFICADO&tipoNuProcesso=UNIFICADO
```


## Como iniciar a aplicação

- Usando gradle através do terminal

No diretório raiz do projeto, rode o comando:
```bash
$ ./gradlew bootRun
```

- Pela IDE (IntelliJ, Eclipse, STS)

Importe o projeto como projeto gradle.
Na classe, WebcrawlerApplication, clique com o botão direito e selecione Run.

- Pelo Jar

```bash
java -jar webcrawler-0.0.1-SNAPSHOT.jar
```

Exemplo de saída:

```bash
2020-10-17 22:22:38.663  INFO 7195 --- [           main] com.jusbrasil.WebcrawlerApplication      : Starting WebcrawlerApplication on note with PID 7195 (/home/elaineprata/projects/webcrawler/build/libs/webcrawler-0.0.1-SNAPSHOT.jar started by elaineprata in /home/elaineprata/projects/webcrawler/build/libs)
2020-10-17 22:22:38.667  INFO 7195 --- [           main] com.jusbrasil.WebcrawlerApplication      : No active profile set, falling back to default profiles: default
2020-10-17 22:22:39.709  INFO 7195 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2020-10-17 22:22:39.724  INFO 7195 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2020-10-17 22:22:39.724  INFO 7195 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.38]
2020-10-17 22:22:39.815  INFO 7195 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2020-10-17 22:22:39.815  INFO 7195 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1062 ms
2020-10-17 22:22:40.147  INFO 7195 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2020-10-17 22:22:40.363  INFO 7195 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2020-10-17 22:22:40.510  INFO 7195 --- [           main] o.apache.activemq.broker.BrokerService   : Using Persistence Adapter: MemoryPersistenceAdapter
2020-10-17 22:22:40.578  INFO 7195 --- [  JMX connector] o.a.a.broker.jmx.ManagementContext       : JMX consoles can connect to service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi
2020-10-17 22:22:40.656  INFO 7195 --- [           main] o.apache.activemq.broker.BrokerService   : Apache ActiveMQ 5.15.13 (localhost, ID:note-46031-1602984160527-0:1) is starting
2020-10-17 22:22:40.661  INFO 7195 --- [           main] o.apache.activemq.broker.BrokerService   : Apache ActiveMQ 5.15.13 (localhost, ID:note-46031-1602984160527-0:1) started
2020-10-17 22:22:40.661  INFO 7195 --- [           main] o.apache.activemq.broker.BrokerService   : For help or more information please see: http://activemq.apache.org
2020-10-17 22:22:40.693  INFO 7195 --- [           main] o.a.activemq.broker.TransportConnector   : Connector vm://localhost started
2020-10-17 22:22:40.749  INFO 7195 --- [           main] com.jusbrasil.WebcrawlerApplication      : Started WebcrawlerApplication in 2.592 seconds (JVM running for 3.052)

```

## Como requisitar a busca por um processo?

No endpoint jusbrasil/process-search é possível fazer um POST onde o body contém o número do processo que se deseja buscar:

``` 
curl --location --request POST 'http://localhost:8080/jusbrasil/process-search' \
--header 'JB_USER_ID: 123' \
--header 'Content-Type: application/json' \
--data-raw '{ "processNumber": "0821901-51.2018.8.12.0001" }'
```

Repare que no header é passado o ID da configuração do usuário que solicita essa busca. É através dessa configuração que a aplicação saberá para onde enviar o resultado.
Para facilitar, quando a aplicação sobe uma config é criada:

```JSON
 {
    "id": 123,
    "name": "Elaine Prata",
    "webhookUrl": "https://jusbrasil-webhook.free.beeceptor.com"
 }
```

Dessa forma, ao passar o ID 123 no header de solicitação de busca por processo, o resultado será enviado para o endpoint de um mock online:

```https://jusbrasil-webhook.free.beeceptor.com```
 
 Para verificar que esse mock realmente foi chamado e consultar o resultado, acesse o console do mock:
 
 ```https://beeceptor.com/console/jusbrasil-webhook```

(Ou você pode criar outras configurações com endpoints específicos para onde a busca deve ser enviada.)

## Entregáveis

- <a href="https://gitlab.com/elaineprata/jusbrasil-crawler/-/tree/master/deliverables/libs" target="blank">Jar</a>

- Coleção <a href="https://gitlab.com/elaineprata/jusbrasil-crawler/-/tree/master/deliverables/postman" target="blank">Postman</a>

- Swagger:
Disponível através da <a href="http://localhost:8080/swagger-ui.html" target="blank">URL</a> da aplicação ou na pasta <a href="https://gitlab.com/elaineprata/jusbrasil-crawler/-/tree/master/deliverables/swagger" target="blank">swagger</a> do projeto.

- Relatório de <a href="https://gitlab.com/elaineprata/jusbrasil-crawler/-/tree/master/deliverables/tests/test" target="blank">testes</a>

- Relatório de <a href="https://gitlab.com/elaineprata/jusbrasil-crawler/-/tree/master/deliverables/jacoco/test/html" target="blank">cobertura</a> de código
 

## Melhorias

- Cobertura de código

- Tratamento de exceções

- etc
